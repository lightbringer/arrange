# arrange

Script to move/resize the current window into a given position in a grid. Originally intended for use with fvwm, but should work in any EWMH-compliant window manager as long as the necessary parameters can be determined.