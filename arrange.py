#!/usr/bin/env python2

# MIT License Copyright (c) 2021 Lightbringer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished
# to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
# OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
# OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Resize and move current (focused) window into a slot in a grid.
# usage: arrange.py th bw ww wh wx wy command args...
#        arrange.py th bw ww wh wx wy grid hd hs vd vs
#        arrange.py th bw ww wh wx wy pseudomax margin
# where:
#   th == titlebar height
#   bw == border width
#   ww, wh, wx, wy == focused window width, height, x, y
#     This is only used to find what the current CRTC is (the one to fit the
#     window into a slot of)
#   command == what to do
#     if command == grid:
#       hd/vd == number of horizontal/vertical divisions (slots)
#       hs/vs == number of horizontal/vertical slot to put window into (1-based)
#     if command == pseudomax:
#       margin == how many extra pixels to leave at top, bottom, left and right
# example:
#   +--+--+--+
#   |  |[]|  |
#   +--+--+--+
#   |  |  |  |
#   +--+--+--+
#   arrange.py $[w.width] $[w.height] $[w.x] $[w.y] 3 2 2 1
# dependencies: xrandr, wmctrl
# should be wm-agnostic as long as the wm is sufficiently ewmh-compliant and
# the current window parameters can be obtained somehow (fvwm can pass them)

import collections
import copy
import re
import subprocess
import sys

################ configuration #####################
# we can't detect struts (for now) and we especially can't detect
# layer 5 windows
# so the area to reserve at the edges must be specified here
edgeReserve = [
  #width height    x     y reserveLeft reserveRight reserveTop reserveBottom
  (1366,   768,    0,    0,          0,         128,         0,           25),  # darkstar
  (1920,  1200,    0,  720,          0,           0,         0,           25),  # adas
  (1920,  1080,    0,    0,          0,         128,         0,           25),  # tomeofpower
# (1366,   768,    0,    0,          0,           0,         0,           25),
]
####################################################

Point = collections.namedtuple('Point', ['x', 'y'])

class Rect(object):
  """Represents a rekt^Wrectangle."""
  def __init__(self, width, height, x, y):
    self.width = width
    self.height = height
    self.x = x
    self.y = y

  def __repr__(self):
    return '(width={}, height={}, x={}, y={})'.format(self.width, self.height, self.x, self.y)

  def containsPoint(self, point):
    """Return True if the point is within self."""
    minx = self.x
    miny = self.y
    maxx = minx + self.width - 1
    maxy = miny + self.height - 1
    return ( (minx <= point.x <= maxx) and (miny <= point.y <= maxy) )

  def centerPoint(self):
    x = self.x + (self.width / 2)
    y = self.y + (self.height / 2)
    return Point(x, y)

  def edgePoint(self, index):
    #   0       1       2
    #   7               3
    #   6       5       4
    if index == 0:
      return Point(self.x, self.y)
    elif index == 1:
      return Point(self.x + (self.width / 2), self.y)
    elif index == 2:
      return Point(self.x + self.width, self.y)
    elif index == 3:
      return Point(self.x + self.width, self.y + (self.height / 2))
    elif index == 4:
      return Point(self.x + self.width, self.y + self.height)
    elif index == 5:
      return Point(self.x + (self.width / 2), self.y + (self.height / 2))
    elif index == 6:
      return Point(self.x, self.y + self.height)
    elif index == 7:
      return Point(self.x, self.y + (self.height / 2))

  def contains(self, toBeContained):
    """Return True if the toBeContained Rect is within self.
    Currently only its center will be checked.
    """
    return self.containsPoint(toBeContained.centerPoint())

  def __eq__(self, other):
    return ( (self.width == other.width) and
             (self.height == other.height) and
             (self.x == other.x) and
             (self.y == other.y) )


class Reserve(object):
  """Represents the edge reservations on a particular CRTC."""
  def __init__(self, rt):
    """rt is the tuple to specify reservations with; for its members, see edgeReserve."""
    self.crtc = Rect( rt[0], rt[1], rt[2], rt[3] )
    self.reserveLeft = rt[4]
    self.reserveRight = rt[5]
    self.reserveTop = rt[6]
    self.reserveBottom = rt[7]

  def matchCrtc(self, crtc):
    """Returns True if crtc is equal to the one this object reserves edges on."""
    return self.crtc == crtc

  def alteredCrtc(self):
    """Returns a Rect altered so windows will be correctly arranged in it,
    keeping the reserved edges clear."""
    left = self.crtc.x + self.reserveLeft
    right = self.crtc.x + self.crtc.width - self.reserveRight
    top = self.crtc.y + self.reserveTop
    bottom = self.crtc.y + self.crtc.height - self.reserveBottom
    return Rect(right - left, bottom - top, left, top)


def getCrtcs():
  """Return a list of Rects, each representing a CRTC, as reported by xrandr."""
  result = []
  xrProc = subprocess.Popen(['xrandr', '--query', '--current'], stdout=subprocess.PIPE)
  for line in xrProc.stdout:
    match = re.search(' connected (?:primary )?([0-9]+)x([0-9]+)\+([0-9]+)\+([0-9]+) ', line)
    if match:
      result.append( Rect(int(match.group(1)), int(match.group(2)),
        int(match.group(3)), int(match.group(4))) )
  return result

def getCurrentCrtc(crtcList, currentWindow):
  """Return the CRTC from the crtcList that contains currentWindow.
  If none of them do, return the default CRTC.
  """
  for crtc in crtcList:
    if crtc.contains(currentWindow): return crtc
  # no CRTC contains the center point. try other points.
  print crtcList
  for i in range(8):
    for crtc in crtcList:
      if crtc.containsPoint(currentWindow.edgePoint(i)): return crtc
  # first is the default for now
  return crtcList[0]

def calculateSlot(crtcOrigin, crtcExtent, divisions, slot):
  """Return an (origin, extent) tuple specifying where the current window should
  fit into the crtc the origin and extent of which were specified.
  The window will occupy the slot-th slot out of divisions.
  (origin, extent) is either (x, width) or (y, height) depending on which axis is
  to be considered."""
  s0 = slot - 1         # 0-based slot index
  extent = crtcExtent / divisions
  origin = crtcOrigin + (extent * s0)
  return (origin, extent)

def moveCurrentWindowTo(r):
  gravity = 0
  mvarg = '{},{},{},{},{}'.format(gravity, r.x, r.y, r.width, r.height)
  subprocess.call(['wmctrl', '-r', ':ACTIVE:', '-e', mvarg])

def compensateForDecorations(targetRect, titleHeight, borderWidth):
  """Return a Rect containing the window coordinates to set that is adjusted for
  decorations so the window is not too wide or too high."""
  corrected = copy.copy(targetRect)
  corrected.width -= 2 * borderWidth
  corrected.height -= (2 * borderWidth) + titleHeight
  return corrected


# preprocess edgeReserve (turn it into a list of Reserve objects)
reserves = map(Reserve, edgeReserve)

# find current CRTC
titleHeight = int(sys.argv[1])
borderWidth = int(sys.argv[2])
currentWindow = Rect( int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5]), int(sys.argv[6]) )
crtc = getCurrentCrtc(getCrtcs(), currentWindow)
if crtc is None: sys.exit(1)

# reserve edges in current CRTC if necessary
for reserve in reserves:
  if reserve.matchCrtc(crtc):
    crtc = reserve.alteredCrtc()
    break

# get command
command = sys.argv[7]

if command == 'grid':
    # calculate slot
    hd = int(sys.argv[8])
    hs = int(sys.argv[9])
    hSlot = calculateSlot(crtc.x, crtc.width, hd, hs)
    vd = int(sys.argv[10])
    vs = int(sys.argv[11])
    vSlot = calculateSlot(crtc.y, crtc.height, vd, vs)

    # where i want the window to be
    targetRect = Rect(hSlot[1], vSlot[1], hSlot[0], vSlot[0])
    correctedRect = compensateForDecorations(targetRect, titleHeight, borderWidth)

    # actually move/resize the window
    moveCurrentWindowTo(correctedRect)
elif command == 'pseudomax':
    margin = int(sys.argv[8])
    targetRect = crtc
    correctedRect = compensateForDecorations(targetRect, titleHeight, borderWidth)
    correctedRect.height -= 2 * margin
    correctedRect.width -= 2 * margin
    correctedRect.x += margin
    correctedRect.y += margin

    # actually move/resize the window
    moveCurrentWindowTo(correctedRect)


